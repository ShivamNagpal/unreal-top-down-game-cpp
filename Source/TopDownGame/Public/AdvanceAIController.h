// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "AdvanceAIController.generated.h"

/**
 * 
 */
UCLASS()
class TOPDOWNGAME_API AAdvanceAIController : public AAIController
{
	GENERATED_BODY()

	UFUNCTION(BlueprintPure, Category="AdvancedAI", meta=(WorldContext="WorldContextObject"))
	virtual UPARAM(DisplayName = "Found") bool FindNextOrb(const UObject* WorldContextObject,
	                                                       TSubclassOf<AActor> ActorClass,
	                                                       FVector Location, AActor*& FoundActor);
};
