// Fill out your copyright notice in the Description page of Project Settings.


#include "AdvanceAIController.h"

#include "Kismet/GameplayStatics.h"
#include <float.h>


bool AAdvanceAIController::FindNextOrb(const UObject* WorldContextObject, TSubclassOf<AActor> ActorClass,
                                       FVector Location, AActor*& FoundActor)
{
	TArray<AActor*> OutActors;
	UGameplayStatics::GetAllActorsOfClass(WorldContextObject, ActorClass, OutActors);
	if (OutActors.IsEmpty())
	{
		return false;
	}
	double MinDistance = DBL_MAX;
	FoundActor = nullptr;
	for (AActor* AActor : OutActors)
	{
		if (const double Distance = FVector::Distance(Location, AActor->GetTransform().GetLocation()); Distance <
			MinDistance)
		{
			MinDistance = Distance;
			FoundActor = AActor;
		}
	}
	return true;
}
